package com.example.testpermohonancuti.dtos;

import java.util.Date;

public class PositionDTO {
	private long positionId;
	private Date createdAt;
	private String createdBy;
	private String positionName;
	private Date updatedAt;
	private String updatedBy;
	
	public PositionDTO() {
		// TODO Auto-generated constructor stub
	}

	public PositionDTO(long positionId, Date createdAt, String createdBy, String positionName, Date updatedAt,
			String updatedBy) {
		super();
		this.positionId = positionId;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.positionName = positionName;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getPositionId() {
		return positionId;
	}

	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
}
