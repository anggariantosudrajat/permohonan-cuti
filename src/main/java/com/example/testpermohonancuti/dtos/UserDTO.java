package com.example.testpermohonancuti.dtos;

import java.util.Date;

public class UserDTO {
	private long userId;
	private PositionDTO position;
	private Date createdAt;
	private String createdBy;
	private Date updatedAt;
	private String updatedBy;
	private String userName;
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserDTO(long userId, PositionDTO position, Date createdAt, String createdBy, Date updatedAt,
			String updatedBy, String userName) {
		super();
		this.userId = userId;
		this.position = position;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
		this.userName = userName;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public PositionDTO getPosition() {
		return position;
	}

	public void setPosition(PositionDTO position) {
		this.position = position;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
