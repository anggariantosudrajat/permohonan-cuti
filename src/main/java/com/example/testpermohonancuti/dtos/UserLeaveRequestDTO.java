package com.example.testpermohonancuti.dtos;

import java.util.Date;

public class UserLeaveRequestDTO {
	private long userLeaveRequestId;
	private UserDTO user;
	private Date createdAt;
	private String createdBy;
	private Date tanggalPengajuan;
	private String description;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private Integer sisaCuti;
	private String submissionStatus;
	private Date updatedAt;
	private String updatedBy;
	
	public UserLeaveRequestDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserLeaveRequestDTO(long userLeaveRequestId, UserDTO user, Date createdAt, String createdBy,
			Date tanggalPengajuan, String description, Date leaveDateFrom, Date leaveDateTo, Integer sisaCuti,
			String submissionStatus, Date updatedAt, String updatedBy) {
		super();
		this.userLeaveRequestId = userLeaveRequestId;
		this.user = user;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.tanggalPengajuan = tanggalPengajuan;
		this.description = description;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.sisaCuti = sisaCuti;
		this.submissionStatus = submissionStatus;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
	}

	public long getUserLeaveRequestId() {
		return userLeaveRequestId;
	}

	public void setUserLeaveRequestId(long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getTanggalPengajuan() {
		return tanggalPengajuan;
	}

	public void setTanggalPengajuan(Date tanggalPengajuan) {
		this.tanggalPengajuan = tanggalPengajuan;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public Integer getSisaCuti() {
		return sisaCuti;
	}

	public void setSisaCuti(Integer sisaCuti) {
		this.sisaCuti = sisaCuti;
	}

	public String getSubmissionStatus() {
		return submissionStatus;
	}

	public void setSubmissionStatus(String submissionStatus) {
		this.submissionStatus = submissionStatus;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
}
