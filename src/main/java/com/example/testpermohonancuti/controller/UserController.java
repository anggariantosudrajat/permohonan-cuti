package com.example.testpermohonancuti.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testpermohonancuti.dtos.UserDTO;
import com.example.testpermohonancuti.exceptions.IdNotFoundException;
import com.example.testpermohonancuti.models.User;
import com.example.testpermohonancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	UserRepository userRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//Create
	@PostMapping("/create")
	public Map<String, Object> createUser(@RequestBody UserDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		User user = modelMapper.map(body, User.class);
		userRepo.save(user);
		body.setUserId(user.getUserId());
		result.put("Message", "Create User Success");
		result.put("Data", body);
		return result;
	}
	
	//Read All
	@GetMapping("/readall")
	public Map<String, Object> readAllUser(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserDTO> listUserDTO = new ArrayList<>();
		List<User> listUser = userRepo.findAll();
		for (User user : listUser) {
			UserDTO userDTO = modelMapper.map(user, UserDTO.class);
			listUserDTO.add(userDTO);
		}
		result.put("Message", "Read All User Success");
		result.put("Total", listUserDTO.size());
		result.put("Data", listUserDTO);
		return result;
	}
	
	//Read Single
	@GetMapping("/readsingle")
	public Map<String, Object> readSingleUser(@RequestParam("userid") Long userId){
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userRepo.findById(userId).orElseThrow(() -> new IdNotFoundException(String.format("User ID %s tidak dapat ditemukan", userId)));
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		result.put("Message", "Read Single User Success");
		result.put("Data", userDTO);
		return result;
	}
	
	//Update
	@PutMapping("/update")
	public Map<String, Object> updateUser(@RequestParam("userid") Long userId, @RequestBody UserDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userRepo.findById(userId).orElseThrow(() -> new IdNotFoundException(String.format("User ID %s tidak dapat ditemukan", userId)));
		body.setUserId(userId);
		user = modelMapper.map(body, User.class);
		userRepo.save(user);
		result.put("Message", "Update User Success");
		result.put("Data", body);
		return result;
	}
	
	//Delete
	@DeleteMapping("/delete")
	public Map<String, Object> deleteUser(@RequestParam("userid") Long userId){
		Map<String, Object> result = new HashMap<String, Object>();
		User user = userRepo.findById(userId).orElseThrow(() -> new IdNotFoundException(String.format("User ID %s tidak dapat ditemukan", userId)));
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		userRepo.delete(user);
		result.put("Message", "Delete User Success");
		result.put("Data", userDTO);
		return result;
	}
}
