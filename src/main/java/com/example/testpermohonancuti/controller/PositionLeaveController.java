package com.example.testpermohonancuti.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.testpermohonancuti.dtos.PositionLeaveDTO;
import com.example.testpermohonancuti.exceptions.IdNotFoundException;
import com.example.testpermohonancuti.models.PositionLeave;
import com.example.testpermohonancuti.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("/api/positionleave")
public class PositionLeaveController {

	@Autowired
	PositionLeaveRepository positionLeaveRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	
	//Create
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@RequestBody PositionLeaveDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = modelMapper.map(body, PositionLeave.class);
		positionLeaveRepo.save(positionLeave);
		body.setPositionLeaveId(positionLeave.getPositionLeaveId());
		result.put("Message", "Create Position Leave Success");
		result.put("Data", body);
		return result;
	}
	
	//Read All
	@GetMapping("/readall")
	public Map<String, Object> readAllPositionLeave(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionLeaveDTO> listPositionLeaveDTO = new ArrayList<>();
		List<PositionLeave> listPositionLeave = positionLeaveRepo.findAll();
		for (PositionLeave positionLeave : listPositionLeave) {
			PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
			listPositionLeaveDTO.add(positionLeaveDTO);
		}
		result.put("Message", "Read All Position Leave Success");
		result.put("Total", listPositionLeaveDTO.size());
		result.put("Data", listPositionLeaveDTO);
		return result;
	}
	
	//Read Single
	@GetMapping("/readsingle")
	public Map<String, Object> readSinglePositionLeave(@RequestParam("positionleaveid") Long positionLeaveId){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepo.findById(positionLeaveId).orElseThrow(() -> new IdNotFoundException(String.format("Position Leave ID %s tidak dapat ditemukan", positionLeaveId)));
		PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
		result.put("Message", "Read Single Position Leave Success");
		result.put("Data", positionLeaveDTO);
		return result;
	}
	
	//Update
	@PutMapping("/update")
	public Map<String, Object> updatePositionLeave(@RequestParam("positionleaveid") Long positionLeaveId, @RequestBody PositionLeaveDTO body){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepo.findById(positionLeaveId).orElseThrow(() -> new IdNotFoundException(String.format("Position Leave ID %s tidak dapat ditemukan", positionLeaveId)));
		body.setPositionLeaveId(positionLeaveId);
		positionLeave = modelMapper.map(body, PositionLeave.class);
		positionLeaveRepo.save(positionLeave);
		result.put("Message", "Update Position Leave Success");
		result.put("Data", body);
		return result;
	}
	
	//Delete
	@DeleteMapping("/delete")
	public Map<String, Object> deletePositionLeave(@RequestParam("positionleaveid") Long positionLeaveId){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepo.findById(positionLeaveId).orElseThrow(() -> new IdNotFoundException(String.format("Position Leave ID %s tidak dapat ditemukan", positionLeaveId)));
		PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
		positionLeaveRepo.delete(positionLeave);
		result.put("Message", "Delete Position Leave Success");
		result.put("Data", positionLeaveDTO);
		return result;
	}
}
