package com.example.testpermohonancuti.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.testpermohonancuti.dtos.UserLeaveRequestDTO;
import com.example.testpermohonancuti.models.PositionLeave;
import com.example.testpermohonancuti.models.User;
import com.example.testpermohonancuti.models.UserLeaveRequest;
import com.example.testpermohonancuti.repositories.PositionLeaveRepository;
import com.example.testpermohonancuti.repositories.UserLeaveRequestRepository;
import com.example.testpermohonancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api")
public class RequestLeaveController {

	@Autowired
	PositionLeaveRepository positionLeaveRepo;
	
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepo;
	
	@Autowired
	UserRepository userRepo;
	
	ModelMapper modelMapper = new ModelMapper();
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	//Max Jatah Cuti
	private int maxJatahCuti(User user) {
		int jatahCuti = 0;
		PositionLeave positionLeave = positionLeaveRepo.findById(user.getPosition().getPositionId()).get();
		jatahCuti = positionLeave.getJatahCuti();
		return jatahCuti;
	}
	
	//Min Jatah Cuti
	private int minJatahCuti(UserLeaveRequest userLeaveRequest, User user) {
		int min = 0;
		List<UserLeaveRequest> listUserLeaveRequests = userLeaveRequestRepo.findAll();
		List<Integer> listInteger = new ArrayList<Integer>();
		for (UserLeaveRequest leaveRequest : listUserLeaveRequests) {
			if (leaveRequest.getUser().getUserId() == user.getUserId()) {
				listInteger.add(leaveRequest.getSisaCuti());
			}
			else {
				return maxJatahCuti(user);
			}
		}
		min = Collections.min(listInteger);
		return min;
	}
	
	//Validate Date
	private boolean validateDate(Date date1, Date date2) {
		boolean fixDate = false;
		Calendar setDate1 = Calendar.getInstance();
		setDate1.setTime(date1);
		Calendar setDate2 = Calendar.getInstance();
		setDate2.setTime(date2);
		if (setDate1.compareTo(setDate2)>0) {
			fixDate = true;
		}
		else {
			fixDate = false;;
		}
		return fixDate;
	}
	
	//Selisih Tanggal
	private int selisihTanggal(Date date1, Date date2) {
		long difference = (date1.getTime() - date2.getTime());
		long differenceDays = difference / (24*60*60*1000);
		int i = (int)differenceDays + 1;
		return i;
	}
	
	//Validate Jatah Cuti
	private boolean validateJatahCuti(UserLeaveRequest userLeaveRequest, User user) {
		boolean result = false;
		if (selisihTanggal(userLeaveRequest.getLeaveDateTo(), userLeaveRequest.getLeaveDateFrom()) <= maxJatahCuti(user)) {
			result = true;
		}
		else {
			result = false;
		}
		return result;
	}
	
	//Calculate Sisa Cuti
	private int sisaCuti(UserLeaveRequest userLeaveRequest, User user) {
		int days = 0;
		List<UserLeaveRequest> listUserLeaveRequest = userLeaveRequestRepo.findAll();
		for (UserLeaveRequest leaveRequest : listUserLeaveRequest) {
			if (leaveRequest.getUser().getUserId() == user.getUserId()) {
				days = minJatahCuti(userLeaveRequest, user) - selisihTanggal(userLeaveRequest.getLeaveDateTo(), userLeaveRequest.getLeaveDateFrom());
			}
			else {
				days = maxJatahCuti(user) - selisihTanggal(userLeaveRequest.getLeaveDateTo(), userLeaveRequest.getLeaveDateFrom());
			}
		}
		return days;
	}
	
	//Validate Sisa Cuti
	private boolean validateSisaCuti(UserLeaveRequest userLeaveRequest, User user) {
		boolean valid = false;
		if (sisaCuti(userLeaveRequest, user)>0) {
			valid = true;
		}
		else {
			valid = false;
		}
		return valid;
	}
	
	//Return Sisa Cuti
	private int returnSisaCuti(UserLeaveRequest userLeaveRequest, User user) {
		int days = 0;
		if (validateSisaCuti(userLeaveRequest, user)) {
			days = sisaCuti(userLeaveRequest, user);
		}
		else {
			days = 0;
		}
		return days;
	}
	
	//Create
	@PostMapping("/requestleave")
	public Map<String, Object> requestLeave(@RequestBody UserLeaveRequestDTO body){
		Map<String, Object> result = new HashMap<>();
		List<UserLeaveRequest> listUserLeaveRequests = new ArrayList<UserLeaveRequest>();
		UserLeaveRequest userLeaveRequest = modelMapper.map(body, UserLeaveRequest.class);
		listUserLeaveRequests.add(userLeaveRequest);
		User user = userRepo.findById(userLeaveRequest.getUser().getUserId()).get();
		userLeaveRequest.setSisaCuti(returnSisaCuti(userLeaveRequest, user));
		//Jatah Cuti Sesuai
		if (validateJatahCuti(userLeaveRequest, user) && 
				validateDate(userLeaveRequest.getLeaveDateTo(), userLeaveRequest.getLeaveDateFrom()) && (minJatahCuti(userLeaveRequest, user) > 0) &&
				(selisihTanggal(userLeaveRequest.getLeaveDateTo(), userLeaveRequest.getLeaveDateFrom()) < minJatahCuti(userLeaveRequest, user))) {
			result.put("Message", "Permohonan Anda sedang diproses");
			userLeaveRequest.setSubmissionStatus("Waiting");
			userLeaveRequestRepo.save(userLeaveRequest);
		}
		
		//Error ketika salah tanggal (Leave Date From > Leave Date To)
		else if (validateDate(userLeaveRequest.getLeaveDateFrom(), userLeaveRequest.getLeaveDateTo())) {
			result.put("Message", "Tanggal yang Anda ajukan tidak valid");
		}
		
		//Error ketika jatah cuti tidak cukup
		else if (selisihTanggal(userLeaveRequest.getLeaveDateTo(), userLeaveRequest.getLeaveDateFrom()) >  minJatahCuti(userLeaveRequest, user)) {
			String dateFrom = dateFormat.format(userLeaveRequest.getLeaveDateFrom());
			String dateTo = dateFormat.format(userLeaveRequest.getLeaveDateTo());
			result.put("Message", String.format("Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal %s"
					+ " sampai %s (%s hari). Jatah cuti Anda yang tersisa adalah %s hari", dateFrom, dateTo,
					selisihTanggal(userLeaveRequest.getLeaveDateTo(), userLeaveRequest.getLeaveDateFrom()), (minJatahCuti(userLeaveRequest, user))));
		}
		
		//Error ketika jatah cuti habis
		else if (minJatahCuti(userLeaveRequest, user) <= 0) {
			result.put("Message", "Mohon maaf, jatah cuti Anda habis");
		}
		
		//Error ketika pengajuan cuti Backdate (Tanggal Pengajuan Cuti < Tanggal Hari Ini)
		else if (validateDate(userLeaveRequest.getTanggalPengajuan(), userLeaveRequest.getLeaveDateFrom())) {
			result.put("Message", "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti Anda");
		}
		return result;
	}
	
}
